# Spark/Zeppelin demo  #

 Spark system을 이해하고 연습하기 위한 가상 작업 환경.

## 기본 자료 ##

 * [Spark summit 2014 Training archive](https://spark-summit.org/2014/training)

 *  [Amp Camp](http://ampcamp.berkeley.edu/big-data-mini-course/)



### Intro to Apache Spark Training - Part 1 ###

  * [Video](https://youtu.be/VWeWViFCzzg?list=PLTPXxbhUt-YWSgAUhrnkyphnh0oKIT8-j)
  * [Slide](http://training.databricks.com/workshop/itas_workshop.pdf)

 * Spark는 무엇?
     * 분산처리 시스템을 만들 수 있는 Framework로 in-memory computing과 순환 data흐름을 지원하는 진보된 DAG([Directed Acylic Graph](http://en.wikipedia.org/wiki/Directed_acyclic_graph) ) 실행 엔진
     * Framework이기 때문에 이것을 이용해서 내가 원하는 일을 올리면 된다.
     * 같은 엔진 안에서 새로운 app을 지원하는 MapReduce를 일반화 하려고 하는 것임.  
     * 언어는 Scala, Python, Java지원. 주로 미는 건 Scala.
          * 내가 Spark를 이용해서 원하는 시스템을 구축하는 것을 코딩하는 것이다.
     * Spark위에서 SQL처리, Streaming, Machine learning, Graph 자료구조 등을 바로 처리 할 수 있다.
          * 물론 각자 개별로는 잘 도는게 있지만 분산처리를 위해 범용적으로 만들어 놓고 각 분야마다 쓸 수 있게 해준게 대단한 일이다.


 * 일반적인 Spark를 가지고 일하는 방식
     * Data 얻고
     * 이 Data처리를 하는 business logic을 작성하고.
     * 실행


 * Spark를 사용한다는 것 = RDD를 이용한다는 것.
     * DryadLINQ-like API
     * 코딩을 하는 것은 실제로 계산 작업이 되는게 아니라 DAG(Directed Acylic Graph)로 디자인 해 나가는 것.
     * [추가자료](http://www.slideshare.net/yongho/rdd-paper-review)

 * RDD? (Resilenct Distrubuted Dataset)
     * 클러스터 전체에서 공유되는 데이터를 추상화 한것.
     * Read-only다. 이래서 분산/병렬처리가 가능하다.
     * Fault tolerance.

 * RDD의 종류, 구성 방법
     * transformation: map, reduce, join등등 data처리 부분
     * action : 각 graph끝에서 결과를 어떤 식으로 내어놓을지를 정의하는 부분.
     * 실제 코드를 작성하는 것을 보면 ``` transformation->transformation->....transformation->action ``` 하는 방식으로 일을 한다.


#### Log mining example ####

```
#!scala

 // load error messages from a log into memory
 // then interactively search for various patterns
 // https://gist.github.com/ceteri/8ae5b9509a08c08a1132

 // base RDD
 val lines = sc.textFile("hdfs://...")

 // transformed RDDs
 val errors = lines.filter(_.startsWith("ERROR"))
 val messages = errors.map(_.split("\t")).map(r => r(1))
 messages.cache()

 // action 1
 messages.filter(_.contains("mysql")).count()

 // action 2
 messages.filter(_.contains("php")).count()

```

#### Wordcount example ####

```
#!scala

  val f = sc.textFile("README.md")
  val wc = f.flatMap(l => l.split(" ")).map(word => (word, 1)).reduceByKey(_ + _)
  wc.saveAsTextFile("wc_out")


```
#### Join example ####

 * ```clk.tsv```: Timestamp - UUID of web page - click수로 구성된 Record.
 * ```reg.tsv```: Timestamp - UUID of web page - other click number



```
#!scala

 val format = new java.text.SimpleDateFormat("yyyy-MM-dd")

 case class Register (d: java.util.Date, uuid: String, cust_id: String, lat: Float, lng: Float)

 case class Click (d: java.util.Date, uuid: String, landing_page: Int)

 val reg = sc.textFile("reg.tsv").map(_.split("\t")).map(
 r => (r(1), Register(format.parse(r(0)), r(1), r(2), r(3).toFloat, r(4).toFloat))
 )

 val clk = sc.textFile("clk.tsv").map(_.split("\t")).map(
 c => (c(1), Click(format.parse(c(0)), c(1), c(2).trim.toInt))
 )

 reg.join(clk).collect()

```



 Output


```

 scala> reg.join(clk).toDebugString
res5: String =
(1) MapPartitionsRDD[20] at join at <console>:32 []
 |  MapPartitionsRDD[19] at join at <console>:32 []
 |  CoGroupedRDD[18] at join at <console>:32 []
 +-(1) MapPartitionsRDD[7] at map at <console>:25 []
 |  |  MapPartitionsRDD[6] at map at <console>:25 []
 |  |  reg.tsv MapPartitionsRDD[5] at textFile at <console>:25 []
 |  |  reg.tsv HadoopRDD[4] at textFile at <console>:25 []
 +-(1) MapPartitionsRDD[11] at map at <console>:25 []
    |  MapPartitionsRDD[10] at map at <console>:25 []
    |  clk.tsv MapPartitionsRDD[9] at textFile at <console>:25 []
    |  clk.tsv HadoopRDD[8] at textFile at <console>:25 []


```

 * 바닥부터 읽어가야 한다.
 * 두개 파일을 각 map에서 읽어와서 join으로 처리하는 Graph를 볼 수 있다.
 * 두개 파일을 Pararell하게 읽어오고 있다.


#### Asignment #####

 Using the README.md and CHANGES.txt
files in the Spark directory:

1. create RDDs to filter each line for the
keyword “Spark”

2. perform a WordCount on each, i.e., so the
results are (K, V) pairs of (word, count)

3. join the two RDDs

Answer
```
#!scala

val file1 = sc.textFile("README.md")
val wc_1 = file1.flatMap(l => l.split(" ")).filter( _ == "Spark").map(word => (word, 1)).reduceByKey(_ + _)


val file2 = sc.textFile("CHANGES.txt")
val wc_2 = file2.flatMap(l => l.split(" ")).filter( _ == "Spark").map(word => (word, 1)).reduceByKey(_ + _)


wc_2.join(wc_1).collect()

```

Output

```
res19: Array[(String, (Int, Int))] = Array((Spark,(60,14)))

```

Debug output

```
scala> wc_2.join(wc_1).toDebugString
res18: String =
(1) MapPartitionsRDD[44] at join at <console>:30 []
 |  MapPartitionsRDD[43] at join at <console>:30 []
 |  CoGroupedRDD[42] at join at <console>:30 []
 |  ShuffledRDD[35] at reduceByKey at <console>:23 []
 +-(1) MapPartitionsRDD[34] at map at <console>:23 []
    |  MapPartitionsRDD[33] at filter at <console>:23 []
    |  MapPartitionsRDD[32] at flatMap at <console>:23 []
    |  CHANGES.txt MapPartitionsRDD[31] at textFile at <console>:21 []
    |  CHANGES.txt HadoopRDD[30] at textFile at <console>:21 []
 |  ShuffledRDD[29] at reduceByKey at <console>:23 []
 +-(1) MapPartitionsRDD[28] at map at <console>:23 []
    |  MapPartitionsRDD[27] at filter at <console>:23 []
    |  MapPartitionsRDD[26] at flatMap at <console>:23 []
    |  README.md MapPartitionsRDD[25] at textFi...

```


### Intro to Apache Spark Training - Part 2 ###

* [Video](https://youtu.be/KsBXyVL2CHk?list=PLTPXxbhUt-YWSgAUhrnkyphnh0oKIT8-j)


### Intro to Apache Spark Training - Part 3 ###

* [Video](https://youtu.be/Cxv_ERVKYE4?list=PLTPXxbhUt-YWSgAUhrnkyphnh0oKIT8-j)


#### Spark context ####
 * First thing that a Spark program does is create a SparkContext object, which tells Spark how to access a cluster

 * In the shell for either Scala or Python, this is the sc variable, which is created automatically.

 * Other programs must use a constructor to instantiate a new SparkContext.

 * Then in turn SparkContext gets used to create other variables.

#### Spark Enssentials:Master ####

## Zeppelin ##
 복잡한 Spark의 기능들에 붙인 UI.  [Homepage](https://zeppelin.incubator.apache.org)


###Start Zeppelin###

```
bin/zeppelin-daemon.sh start
```

###Stop Zeppelin###

```
bin/zeppelin-daemon.sh stop
```

###Connect Zeppelin###
```
http://192.168.11.187:8080/
```

###Zepplelin에서 pyspark이용하기##

 현재 Zepplelin 화면상의 interpreter 메뉴에서 zeppelin.pyspark.python 에 ```/home/vagrant/spark/bin/pyspark``` 입력해주시면 동작합니다. 설정 바꾼 후 sudo bin/zeppelin-daemon.sh restart 해주시면 됩니다. 
 > 다른 환경이신 분들은 spark내의 pyspark경로를 써주시면 됩니다.